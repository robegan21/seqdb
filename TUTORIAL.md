SeqDB Tutorial
==============

Start with a FASTQ file of your choosing (we will call it test.fq), and use
the profile command to get a histogram of the sequence and ID lengths:

    $ seqdb profile test.fq
    seqdb-profile: profiling FASTQ records from 'test.fq'
    fastq: opening file 'test.fq' with size 2585658
      total reads:        10000
      ID lengths:
          53         4342
          54         5658
      sequence lengths:
         100        10000

The output shows that for this file, all sequences have a fixed length of 100
bp and the maximum ID length is 54 characters.

With this information, we can now compress the FASTQ file into a SeqDB file:

    $ seqdb compress 100 54 test.fq test.seqdb
    seqdb-compress: adding FASTQ records from 'test.fq'
    fastq: opening file 'test.fq' with size 2585658
    seqdb: using BLOSC 1.1.3 ($Date:: 2010-11-16 #$) with 2 thread(s)
    seqdb-compress: added 10000 records

By default, SeqDB uses as many threads as your system has cores. In this case,
running on a dual-core MacBook Pro laptop used 2 threads.

We can mount the SeqDB file as a FASTQ file using the mount command:

    $ seqdb mount test.seqdb test-mount.fq &

You will immediately see a message indicating that the mount has been started
as a background process and the SeqDB file has been opened:

    seqdb-mount: mounting test.seqdb at test-mount.fq
    seqdb: using BLOSC 1.1.3 ($Date:: 2010-11-16 #$) with 2 thread(s)
    seqdb: found 10000 records at 'test.seqdb:/seqdb'
    seqdb: sequence length is 100, ID length is 54

Press enter to return to the prompt.

You can browse the records in the mounted FASTQ file using the more command
(with -f, since the mount path is a 'special' file):

    $ more -f test-mount.fq

Now hit the 'q' key and enter, to quit more. This will end the access to the
mounted file (sending a signal called 'SIGPIPE') and you will see a message
that it has been automatically unmounted:

    seqdb-mount: caught SIGPIPE
    seqdb-mount: unmounting test-mount.fq

The mount will also be cleaned up when you reach the end of the file, or if the
background process ends (e.g. if it is killed, or you interrupt it with
CTRL-C).

