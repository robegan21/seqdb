/*
 * SeqDB - storage model for Next Generation Sequencing data
 *
 * Copyright 2011-2014, Brown University, Providence, RI. All Rights Reserved.
 *
 * This file is part of SeqDB.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
#ifdef MPIPARALLEL
#include <mpi.h>
#endif
#include <omp.h>

#include "seqdb.h"

#include "util.h"

#ifdef MPIPARALLEL
#include <mpi.h>
#endif

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: seqdb-extract [-i ID] SEQDB\n"
"\n"
"Converts the SEQDB input file to FASTQ format, printing to stdout.\n"
"\n"
"  -i  extract only the record at the 1-based index ID\n"
"\n"
"Example usage:\n"
"seqdb-extract 1.seqdb >1.fastq\n"
"seqdb-extract -i 1705 1.seqdb\n"
	<< endl;
}

int main(int argc, char** argv)
{
    int myrank = 0;
    size_t id = 0;
#ifdef MPIPARALLEL
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
#endif

	int c;
	while ((c = getopt(argc, argv, "i:hv")) != -1)
		switch (c) {
			case 'i':
				id = (size_t)strtoul(optarg, NULL, 10);
				break;
			case 'v':
                if (!myrank)
                    PRINT_VERSION;
                #ifdef MPIPARALLEL
                MPI_Finalize();
                #endif
				return EXIT_SUCCESS;
			case 'h':
			default:
                if (!myrank)
                    print_usage();
                #ifdef MPIPARALLEL
                MPI_Finalize();
                #endif
				return EXIT_SUCCESS;
		}

	if (optind >= argc)
		ARG_ERROR("you must specify an input file");

    double start_t = omp_get_wtime();

	/* Open the input. */
	SeqDB* db = SeqDB::open(argv[optind]);

	if (id > 0) {
		/* Read a single record */
		Sequence seq;
		db->readAt(id, seq);
		cout << seq;
	} else {
#ifdef MPIPARALLEL
        string filename = argv[optind];
        ostringstream out;
        out << myrank;
        string suffix = out.str() + ".fq";
        filename = filename + suffix;
        FILE * ifp = fopen(filename.c_str(), "w");
        db->exportFASTQ(ifp);
#else
		/* Dump all records, and adjust stdout buffer for efficiency */
		ios_base::sync_with_stdio(false);
		setvbuf(stdout, NULL, _IOFBF, 1024*1024);
		db->exportFASTQ(stdout);
#endif
	}

	/* Cleanup. */
	delete db;

    if (!myrank)
        NOTIFY(setiosflags(ios::fixed) << setprecision(1) 
               << "Time taken: " << omp_get_wtime() - start_t << "s");
#ifdef MPIPARALLEL
    MPI_Finalize();
#endif

	return EXIT_SUCCESS;
}

