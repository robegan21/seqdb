#ifndef __SEQDB_H__
#define __SEQDB_H__

#include <stdio.h>
#include <string>
#include <vector>
#include "seqdb-version.h"
#include "seqpack.h"
#include "seq.h"
#include "fastq.h"

using namespace std;

class SeqDB {

	public:
		/* functions */
        SeqDB(const char* filename, char mode, size_t slen, size_t ilen, 
              size_t max_records = 0, int use_collective_io = 0);
		static SeqDB* open(const char* filename);
		static SeqDB* create(const char* filename, char mode, size_t slen, 
                             size_t ilen, size_t max_records, 
                             int use_collective_io);
		size_t size() { return nrecords; }
		size_t getSeqLength() { return slen; }
		size_t getIDLength() { return ilen; }

		/* virtual functions */
		virtual ~SeqDB();
		virtual void write(const Sequence& seq) = 0;
		virtual bool read(Sequence& seq) = 0;
		virtual void readAt(size_t i, Sequence& seq) = 0;
		virtual void importFASTQ(FASTQ* f) = 0;
        #ifdef MPIPARALLEL
		virtual void mpi_importFASTQ(char *input_name) = 0;
        #endif
		virtual void exportFASTQ(FILE* f) = 0;

		/* function by Aydin */
		virtual void fill_block(vector<string> & seqs, vector<string> & quals, size_t count) = 0;
		virtual void fill_block(vector<string> & seqs, vector<string> & quals, vector<string> & ids, size_t count) = 0;
		/* end functions by Aydin */

		virtual size_t getBlockSize() = 0;

		/* data */
		static const char READ = 0x00;
		static const char TRUNCATE = 0x01;
		static const char APPEND = 0x02;

	protected:
		/* data */
		const char* filename;
        size_t max_records;
		size_t nrecords;
		size_t nread;
		size_t slen;
		size_t ilen;
		int qual_offset;
		char mode;
		SeqPack* pack;
};

#endif

