/*
 * SeqDB - storage model for Next Generation Sequencing data
 *
 * Copyright 2011-2014, Brown University, Providence, RI. All Rights Reserved.
 *
 * This file is part of SeqDB.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <omp.h>
#include <sys/stat.h>
#ifdef MPIPARALLEL
#include <mpi.h>
#endif
#include <iomanip>

#include "blosc.h"
#include "blosc_filter.h"
#include "h5seqdb.h"

#define PROGNAME "seqdb"
#include "util.h"

#define BLOSC_LEVEL 4

#ifndef H5SEQDB_MALLOC_ALIGN
#define H5SEQDB_MALLOC_ALIGN 64
#endif

#ifndef H5SEQDB_MALLOC_PAD
#define H5SEQDB_MALLOC_PAD H5SEQDB_MALLOC_ALIGN
#endif

#define H5SEQDB_SEQ_DATASET "seq"
#define H5SEQDB_ID_DATASET "id"
#define H5SEQDB_GROUP "/seqdb"

#define H5SEQDB_ENC_BASE_ATTR "__encode_base__"
#define H5SEQDB_ENC_QUAL_ATTR "__encode_qualilty__"
#define H5SEQDB_DEC_BASE_ATTR "__decode_base__"
#define H5SEQDB_DEC_QUAL_ATTR "__decode_qualilty__"

#define H5CHK(status) do {                                          \
        if (status < 0)                                             \
            ERROR("HDF5 error at " << __FILE__ << ":" << __LINE__); \
    } while (0)

#define H5TRY(call) do {                        \
        herr_t status = (herr_t)(call);         \
        H5CHK(status);                          \
    } while(0)

#define H5LOC(file,path) "'" << file << ":" << path << "'"

using namespace std;

herr_t hdf5_exit_on_error (hid_t estack_id, void *unused)
{
	H5Eprint(estack_id, stderr);
	exit(EXIT_FAILURE);
}

void H5SeqDB::open_file(const char* filename, hid_t mode)
{
	/* Property lists. */
	hid_t fapl = H5P_DEFAULT;
	//hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
	//H5CHK(fapl)
	//H5Pset_alignment(fapl, blocksize, 0);
	//H5Pset_meta_block_size(fapl, blocksize);

	/* Open. */
	h5file = H5Fopen(filename, mode, fapl);
	if (h5file < 0) ERROR("cannot open file '" << filename << "'");

	/* Cleanup. */
	H5TRY(H5Pclose(fapl));
}

void H5SeqDB::open_datasets(const char* path)
{
	/* Property lists. */
	hid_t dapl = H5P_DEFAULT;
	//hid_t dapl = H5Pcreate(H5P_DATASET_ACCESS);
	//H5CHK(dapl)
	//H5Pset_chunk_cache(dapl, );

	/* Open. */
	hid_t group = H5Gopen(h5file, path, H5P_DEFAULT);
	if (group < 0) ERROR("cannot open group at " << H5LOC(filename, path));

	sdset = H5Dopen(group, H5SEQDB_SEQ_DATASET, dapl);
	if (sdset < 0) ERROR("cannot open sequence dataset at " << H5LOC(filename, path));

	idset = H5Dopen(group, H5SEQDB_ID_DATASET, dapl);
	if (idset < 0) ERROR("cannot open ID dataset at " << H5LOC(filename, path));

	/* Get data space. */
	sspace = H5Dget_space(sdset);
	H5CHK(sspace);
	ispace = H5Dget_space(idset);
	H5CHK(ispace);

	/* Get dimensions. */
	hsize_t sdims[2] = { 0, 0 };
	hsize_t idims[2] = { 0, 0 };
	H5TRY(H5Sget_simple_extent_dims(sspace, sdims, NULL));
	H5TRY(H5Sget_simple_extent_dims(ispace, idims, NULL));
	if (sdims[0] != idims[0])
		ERROR("sequence and ID datasets at " << H5LOC(filename, path) <<
              "have different sizes: corrupted file?");
	nrecords = sdims[0];
	slen = sdims[1];
	ilen = idims[1];
	if (!myrank) {
		NOTIFY("Found " << nrecords << " records at " << H5LOC(filename, path));
		NOTIFY("Sequence length is " << slen << ", ID length is " << ilen);
	}

	/* Read SeqPack tables from attributes. */
	uint8_t enc[SEQPACK_ENC_SIZE];
	read_array_attribute(H5SEQDB_ENC_BASE_ATTR,
			H5T_NATIVE_UINT8, SEQPACK_ENC_SIZE, enc);
	pack->setEncBase(enc, SEQPACK_ENC_SIZE);
	read_array_attribute(H5SEQDB_ENC_QUAL_ATTR,
			H5T_NATIVE_UINT8, SEQPACK_ENC_SIZE, enc);
	pack->setEncQual(enc, SEQPACK_ENC_SIZE);

	char dec[SEQPACK_DEC_SIZE];
	read_array_attribute(H5SEQDB_DEC_BASE_ATTR,
			H5T_NATIVE_CHAR, SEQPACK_DEC_SIZE, dec);
	pack->setDecBase(dec, SEQPACK_DEC_SIZE);
	read_array_attribute(H5SEQDB_DEC_QUAL_ATTR,
			H5T_NATIVE_CHAR, SEQPACK_DEC_SIZE, dec);
	pack->setDecQual(dec, SEQPACK_DEC_SIZE);

	/* Cleanup. */
	//H5TRY(H5Pclose(dapl));
	H5TRY(H5Gclose(group));
}

void H5SeqDB::read_array_attribute(
		const char* name,
		hid_t type,
		hsize_t n,
		void* array)
{
	hid_t attr = H5Aopen(h5file, name, H5P_DEFAULT);
	H5CHK(attr);

	H5TRY(H5Aread(attr, type, array));

	/* Cleanup. */
	H5TRY(H5Aclose(attr));
}

void H5SeqDB::create_file(const char* filename, hid_t mode)
{
	/* Property lists. */
	hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
	H5CHK(fapl);
    // pad out chunks to stripe size of 1M
	//H5CHK(H5Pset_alignment(fapl, 0, 1024 * 1024));
	//H5Pset_meta_block_size(fapl, blocksize);
    #ifdef MPIPARALLEL
	if (use_collective_io)
		H5CHK(H5Pset_fapl_mpio(fapl, MPI_COMM_WORLD, MPI_INFO_NULL));
    #endif
	/* Create. */
#ifdef MPIPARALLEL
	MPI_Info info  = MPI_INFO_NULL;
	H5Pset_fapl_mpio(fapl, MPI_COMM_WORLD, info);
#endif
	
	h5file = H5Fcreate(filename, mode, H5P_DEFAULT, fapl);
	if (h5file < 0) 
		PERROR("cannot create file '" << filename << "'");

	/* Cleanup. */
	H5TRY(H5Pclose(fapl));
}

void H5SeqDB::create_datasets(const char* path)
{
	/* Property lists. */
	hid_t sdcpl = H5Pcreate(H5P_DATASET_CREATE);
	H5CHK(sdcpl);
	hid_t idcpl = H5Pcreate(H5P_DATASET_CREATE);
	H5CHK(idcpl);
	H5TRY(H5Pset_layout(sdcpl, H5D_CHUNKED));
	H5TRY(H5Pset_layout(idcpl, H5D_CHUNKED));
	hsize_t sdims[2] = { blocksize, slen };
	hsize_t idims[2] = { blocksize, ilen };
	H5TRY(H5Pset_chunk(idcpl, 2, idims));
	H5TRY(H5Pset_chunk(sdcpl, 2, sdims));

    if (!use_collective_io) {
        // only use BLOSC compression if not doing collective MPI writes
        unsigned cd[6];
        cd[4] = BLOSC_LEVEL;
        cd[5] = 0;
        H5TRY(H5Pset_filter(idcpl, FILTER_BLOSC, H5Z_FLAG_OPTIONAL, 6, cd));
        H5TRY(H5Pset_filter(sdcpl, FILTER_BLOSC, H5Z_FLAG_OPTIONAL, 6, cd));
	}
	/* Dimensions. */
	sdims[0] = 1;
	idims[0] = 1;
	hsize_t smaxdims[2] = { H5S_UNLIMITED, slen };
	hsize_t imaxdims[2] = { H5S_UNLIMITED, ilen };
	sspace = H5Screate_simple(2, sdims, smaxdims);
	H5CHK(sspace);
	ispace = H5Screate_simple(2, idims, imaxdims);
	H5CHK(ispace);

	/* Create. */
	hid_t group = H5Gcreate(h5file, path, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	H5CHK(group);
	sdset = H5Dcreate(group,
                      H5SEQDB_SEQ_DATASET,
                      H5T_NATIVE_UINT8,
                      sspace, H5P_DEFAULT, sdcpl, H5P_DEFAULT);
	H5CHK(sdset);
	idset = H5Dcreate(group,
                      H5SEQDB_ID_DATASET,
                      H5T_NATIVE_CHAR,
                      ispace, H5P_DEFAULT, idcpl, H5P_DEFAULT);
	H5CHK(idset);

	/* Store SeqPack tables as attributes. */
	write_array_attribute(H5SEQDB_ENC_BASE_ATTR,
			H5T_NATIVE_UINT8, SEQPACK_ENC_SIZE, pack->getEncBase());
	write_array_attribute(H5SEQDB_ENC_QUAL_ATTR,
			H5T_NATIVE_UINT8, SEQPACK_ENC_SIZE, pack->getEncQual());
	write_array_attribute(H5SEQDB_DEC_BASE_ATTR,
			H5T_NATIVE_CHAR, SEQPACK_DEC_SIZE, pack->getDecBase());
	write_array_attribute(H5SEQDB_DEC_QUAL_ATTR,
			H5T_NATIVE_CHAR, SEQPACK_DEC_SIZE, pack->getDecQual());

	/* Cleanup. */
	H5TRY(H5Pclose(sdcpl));
	H5TRY(H5Pclose(idcpl));
	H5TRY(H5Gclose(group));
}

void H5SeqDB::write_array_attribute(
		const char* name,
		hid_t type,
		hsize_t n,
		const void* array)
{
	hid_t space;
	hid_t attr;

	space = H5Screate_simple(1, &n, NULL);
	H5CHK(space);

	attr = H5Acreate(h5file, name, type, space, H5P_DEFAULT, H5P_DEFAULT);
	H5CHK(attr);

	H5TRY(H5Awrite(attr, type, array));

	/* Cleanup. */
	H5TRY(H5Aclose(attr));
	H5TRY(H5Sclose(space));
}

static double parpack_t = 0;

// note: the remainder is either the block size, or for the last block, 
// what is left to the end of the file. So this makes it simple for
// parallel IO.
void H5SeqDB::flush_writes(hsize_t remainder)
{
	double t;
#ifdef DEBUG
	NOTIFY("flushing " << remainder << " writes at offset " << nrecords);
#endif
//    printf("rank %d flushing %llu writes at offset %ld\n", myrank, remainder, nrecords);

    size_t write_pos = 0;

	/* Property lists. */
	hid_t dxpl = H5P_DEFAULT;
	if (use_collective_io) {
        #ifdef MPIPARALLEL
		dxpl = H5Pcreate(H5P_DATASET_XFER);
		H5CHK(H5Pset_dxpl_mpio(dxpl, H5FD_MPIO_COLLECTIVE));
        size_t records_per_rank = (max_records + num_ranks - 1) / num_ranks;
        write_pos = records_per_rank * myrank;
        #endif
	} else {
        /* Extend. */
        hsize_t scount[2] = { write_pos + nrecords, slen };
        hsize_t icount[2] = { write_pos + nrecords, ilen };
        H5TRY(H5Dextend(sdset, scount));
        H5TRY(H5Dextend(idset, icount));
    }

	/* Dataspace. */
	sspace = H5Dget_space(sdset);
	H5CHK(sspace);
	ispace = H5Dget_space(idset);
	H5CHK(ispace);

	/* Select. */
	hsize_t offset[2] = { write_pos + nrecords - remainder, 0 };
    hsize_t scount[2] = { remainder, slen };
    hsize_t icount[2] = { remainder, ilen };
	H5TRY(H5Sselect_hyperslab(sspace, H5S_SELECT_SET, offset, NULL, scount, NULL));
	H5TRY(H5Sselect_hyperslab(ispace, H5S_SELECT_SET, offset, NULL, icount, NULL));
	offset[0] = 0;
	H5TRY(H5Sselect_hyperslab(smemspace, H5S_SELECT_SET, offset, NULL, scount, NULL));
	H5TRY(H5Sselect_hyperslab(imemspace, H5S_SELECT_SET, offset, NULL, icount, NULL));
	
	t = omp_get_wtime();	
	/* Pack. */
	pack->parpack(remainder, sbuf, pbuf);
	parpack_t += (omp_get_wtime() - t);

	/* Write. */
	H5TRY(H5Dwrite(sdset, H5T_NATIVE_UINT8, smemspace, sspace, dxpl, pbuf));
	H5TRY(H5Dwrite(idset, H5T_NATIVE_CHAR, imemspace, ispace, dxpl, ibuf));
	clear_buffers();

	/* Cleanup. */
	H5TRY(H5Sclose(sspace));
	H5TRY(H5Sclose(ispace));
}

void H5SeqDB::flush_reads()
{
    int myremaining, mybegin;
#ifdef MPIPARALLEL
    //int perproc = nrecords / nprocs;
    //int perproc = ((nrecords / num_ranks) % 2 == 0) ? nrecords / num_ranks : nrecords / num_ranks + 1 ;
	int perproc = (nrecords + num_ranks - 1) / num_ranks;
    if (myrank != num_ranks - 1)
        myremaining = perproc - nread;
    else
        myremaining = (nrecords - perproc * (num_ranks - 1)) - nread;
    mybegin = myrank * perproc;
#else
    myremaining = nrecords - nread;
    mybegin = 0;
#endif

	hsize_t remainder = blocksize;
	if (myremaining < blocksize) 
		remainder = myremaining;

	/* Property lists. */
	hid_t dxpl = H5P_DEFAULT;

	// ABAB: We're using a block distribution of reads here (we could have used a cyclic distribution via 'stride')
	// nrecords = sdims[0];
	// slen = sdims[1];
	// NOTIFY("found " << nrecords << " records at " << H5LOC(filename, path))
	// NOTIFY("sequence length is " << slen << ", ID length is " << ilen)
	
	/* Select. */
	hsize_t offset[2] = { mybegin + nread, 0 };
	hsize_t scount[2] = { remainder, slen };
	hsize_t icount[2] = { remainder, ilen };

	H5TRY(H5Sselect_hyperslab(sspace, H5S_SELECT_SET, offset, NULL, scount, NULL));
	H5TRY(H5Sselect_hyperslab(ispace, H5S_SELECT_SET, offset, NULL, icount, NULL));
	offset[0] = 0;
	H5TRY(H5Sselect_hyperslab(smemspace, H5S_SELECT_SET, offset, NULL, scount, NULL));
	H5TRY(H5Sselect_hyperslab(imemspace, H5S_SELECT_SET, offset, NULL, icount, NULL));

	/* Read. */
	clear_buffers();
	H5TRY(H5Dread(sdset, H5T_NATIVE_UINT8, smemspace, sspace, dxpl, pbuf));
	H5TRY(H5Dread(idset, H5T_NATIVE_CHAR, imemspace, ispace, dxpl, ibuf));

	/* Unpack. */
	pack->parunpack(remainder, pbuf, sbuf);
}




void H5SeqDB::read_at(size_t i)
{
	/* Property lists. */
	hid_t dxpl = H5P_DEFAULT;

	/* Select. */
	hsize_t offset[2] = { i, 0 };
	hsize_t scount[2] = { 1, slen };
	hsize_t icount[2] = { 1, ilen };
	H5TRY(H5Sselect_hyperslab(sspace, H5S_SELECT_SET, offset, NULL, scount, NULL));
	H5TRY(H5Sselect_hyperslab(ispace, H5S_SELECT_SET, offset, NULL, icount, NULL));
	offset[0] = 0;
	H5TRY(H5Sselect_hyperslab(smemspace, H5S_SELECT_SET, offset, NULL, scount, NULL));
	H5TRY(H5Sselect_hyperslab(imemspace, H5S_SELECT_SET, offset, NULL, icount, NULL));

	/* Read. */
	clear_buffers_at();
	H5TRY(H5Dread(sdset, H5T_NATIVE_UINT8, smemspace, sspace, dxpl, pbuf_at));
	H5TRY(H5Dread(idset, H5T_NATIVE_CHAR, imemspace, ispace, dxpl, ibuf_at));

	/* Unpack. */
	pack->unpack(pbuf_at, sbuf_at, sbuf_at + slen);
}



void H5SeqDB::clear_buffers()
{
	memset(pbuf, 0x00, pbufsize);
	memset(sbuf, 0x00, sbufsize);
	memset(ibuf, 0x00, ibufsize);
}

void H5SeqDB::clear_buffers_at()
{
	memset(pbuf_at, 0x00, slen);
	memset(sbuf_at, 0x00, 2 * slen);;
	memset(ibuf_at, 0x00, ilen);
}

void H5SeqDB::alloc()
{
	pbufsize = slen * blocksize;
	CHECK_ERR(
		posix_memalign((void**)&pbuf,
                       H5SEQDB_MALLOC_ALIGN, pbufsize + H5SEQDB_MALLOC_PAD));

	sbufsize = 2 * slen * blocksize;
	CHECK_ERR(
		posix_memalign((void**)&sbuf,
                       H5SEQDB_MALLOC_ALIGN, sbufsize + H5SEQDB_MALLOC_PAD));

	ibufsize = ilen * blocksize;
	CHECK_ERR(
		posix_memalign((void**)&ibuf,
                       H5SEQDB_MALLOC_ALIGN, ibufsize + H5SEQDB_MALLOC_PAD));

	CHECK_ERR(
		posix_memalign((void**)&pbuf_at,
                       H5SEQDB_MALLOC_ALIGN, slen + H5SEQDB_MALLOC_PAD));

	CHECK_ERR(
		posix_memalign((void**)&sbuf_at,
                       H5SEQDB_MALLOC_ALIGN, 2*slen + H5SEQDB_MALLOC_PAD));

	CHECK_ERR(
		posix_memalign((void**)&ibuf_at,
                       H5SEQDB_MALLOC_ALIGN, ilen + H5SEQDB_MALLOC_PAD));
}

/* public functions */

H5SeqDB::H5SeqDB(
		const char* filename,
		char mode,
		size_t slen,
		size_t ilen,
		size_t blocksize,
        size_t max_records,
        int use_collective_io)
	: blocksize(blocksize), SeqDB(filename, mode, slen, ilen, max_records, use_collective_io)
{
	//H5Eset_auto(H5E_DEFAULT, hdf5_exit_on_error, NULL);
    this->use_collective_io = use_collective_io;
    
	myrank = 0;
	num_ranks = 1;
    #ifdef MPIPARALLEL
    MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    #endif    
	
    if (use_collective_io) {
        if (!myrank) {
            NOTIFY("Using collective MPI IO to write to a single file, " << filename);
            NOTIFY("Disabling BLOSC (collective writes don't support filters");
        }
    } else {
        char* version;
        char* date;
        register_blosc(&version, &date);
        int threads = omp_get_max_threads();
        blosc_set_nthreads(threads);
        if (!myrank)
            NOTIFY("Using BLOSC " << version << " (" << date << ") with " 
                   << threads << " threads");
    }
	if (mode == READ) {
		open_file(filename, H5F_ACC_RDONLY);
		/* Opening the datasets will update slen and ilen. */
		open_datasets(H5SEQDB_GROUP);
		nread = 0;
		pack->setLength(this->slen);
	} else {
		if (mode == TRUNCATE) {
            if (!use_collective_io && num_ranks > 1) {
                if (!myrank)
                    NOTIFY("Writing independently to " << num_ranks << " files");
                char fname_for_rank[255];
                char fname[255];
                strcpy(fname, filename);
                char *ext = strrchr(fname, '.');
                if (ext) {
                    // add number before the extension
                    ext[0] = '\0';
                    sprintf(fname_for_rank, "%s.%d.%s", fname, myrank, ext + 1);
                } else {
                    sprintf(fname_for_rank, "%s.%d", fname, myrank);
                } 
                create_file(fname_for_rank, H5F_ACC_TRUNC);
            } else {
                create_file(filename, H5F_ACC_TRUNC);
            }
		} else {
			open_file(filename, H5F_ACC_RDWR);
		}
		create_datasets(H5SEQDB_GROUP);
		nrecords = 0;
	}

	if (this->slen <= 0 || this->ilen <= 0)
		ERROR("sequence or ID length is non-positive");

	alloc();    // ABAB: Creates buffers (pbuf,ibuf) to fill

	hsize_t sdims[2] = { blocksize, this->slen };
	smemspace = H5Screate_simple(2, sdims, NULL);
	H5CHK(smemspace);

	hsize_t idims[2] = { blocksize, this->ilen };
	imemspace = H5Screate_simple(2, idims, NULL);
	H5CHK(imemspace);
}

H5SeqDB::~H5SeqDB()
{
	if (mode == READ) {
		H5TRY(H5Sclose(sspace));
		H5TRY(H5Sclose(ispace));
	} else {
		/* Flush any remaining records. */
		/*
		hsize_t remainder = nrecords % blocksize;
		if (remainder) 
			flush_writes(remainder);
		*/
	}
	free(pbuf);
	free(sbuf);
	free(ibuf);
	free(pbuf_at);
	free(sbuf_at);
	free(ibuf_at);
	H5TRY(H5Sclose(smemspace));
	H5TRY(H5Sclose(imemspace));
	H5TRY(H5Dclose(sdset));
	H5TRY(H5Dclose(idset));
	H5TRY(H5Fclose(h5file));
}

void H5SeqDB::write(const Sequence& seq)
{
	if (ilen < seq.idline.size())
		ERROR("seq.idline is too large (" << seq.idline.size() <<
		      ") for write (" << ilen << ")");
	if (slen < seq.seq.size())
		ERROR("seq.seq is too large (" << seq.seq.size() <<
		      ") for write (" << slen << ")");
	if (slen < seq.qual.size())
		ERROR("seq.idline is too large (" << seq.qual.size() <<
		      ") for write (" << slen << ")");

	hsize_t remainder = nrecords % blocksize;

	/* Copy idline. */
	char* i = ibuf + ilen * remainder;
	memcpy(i, seq.idline.data(), min(ilen, seq.idline.size()));

	/* Copy seq/qual. */
	char* s = sbuf + 2 * slen * remainder;
	memcpy(s, seq.seq.data(), min(slen, seq.seq.size()));
	memcpy(s + slen, seq.qual.data(), min(slen, seq.qual.size()));

	/* Advance the record counter, and flush if the write bufer is full. */
	nrecords++;
	if (nrecords % blocksize == 0) flush_writes(blocksize);
}

bool H5SeqDB::read(Sequence& seq)
{
	hsize_t remainder = nread % blocksize;

	/* Update buffer if we're back to the first record. */
	if (remainder == 0) flush_reads();

	const char* i = ibuf + ilen * remainder;
	seq.idline.assign(i, strnlen(i, ilen));

	const char* s = sbuf + 2 * slen * remainder;
	seq.seq.assign(s, strnlen(s, slen));
	seq.qual.assign(s + slen, strnlen(s + slen, slen));

	/* Advance the read counter. */
	nread++;

	if (nread <= nrecords) return true;
	else return false;
}

void H5SeqDB::readAt(size_t i, Sequence& seq)
{
	if (i >= nrecords)
		ERROR("can't read record " << i << ": only " << nrecords <<
		      " records exist");

	read_at(i);

	seq.idline.assign(ibuf_at, strnlen(ibuf_at, ilen));
	seq.seq.assign(sbuf_at, strnlen(sbuf_at, slen));
	seq.qual.assign(sbuf_at + slen, strnlen(sbuf_at + slen, slen));
}

void H5SeqDB::importFASTQ(FASTQ* f)
{
	double t, write_t = 0;
	double start_t = omp_get_wtime();
	while (f->good())
	{
		hsize_t remainder = nrecords % blocksize;

		/* Read idline. */
		char* i = ibuf + ilen * remainder;
		if (!f->next_line(i, ilen)) break;

		f->check_delim(i[0]);

		/* Read sequence. */
		char* s = sbuf + 2 * slen * remainder;
		if (!f->next_line(s, slen)) break;

		f->check_plus();

		/* Read quality. */
		if (!f->next_line(s + slen, slen)) break;

		/* Advance the record counter, and flush if the write bufer is full. */
		nrecords++;
		if (nrecords % blocksize == 0) {
			t = omp_get_wtime();
			flush_writes(blocksize);
			write_t += (omp_get_wtime() - t);
		}
	}

	hsize_t remainder = nrecords % blocksize;
	if (remainder) {
		t = omp_get_wtime();
        flush_writes(remainder);
		write_t += (omp_get_wtime() - t);
	}

    NOTIFY(setiosflags(ios::fixed) << setprecision(2) 
           << __func__ << " took " << (omp_get_wtime() - start_t) 
           << "s, flush writes took " << write_t << "s, parpack took "
           << parpack_t << "s");
}


#ifdef MPIPARALLEL

//#define DBG_ON
#ifdef DBG_ON
#define DBG(fmt,...) do {                            \
        printf(fmt, ##__VA_ARGS__);                  \
    } while (0)
#else
#define DBG(fmt,...)
#endif

// returns size of line, 0 for a partial line, and -1 for an error
int H5SeqDB::next_fastq_line(char *dest, char *src, int count)
{
    // find the end of line in the src
	char *c = (char*)memchr(src, '\n', count + 1);
	if (c == NULL) {
        if (strlen(src) <= count)
            // cut off line
            return 0;
        // now the line is too long
        return -1;
    }
	int n = c - src + 1;
	memcpy(dest, src, n - 1);
	fastq_lines++;
	return n;
}

const char FASTQ_DELIM = '@';

#define CHK_FQ_LEN(len, count) do {                                     \
        if (len == -1) {                                                \
            if (fail_on_err)                                            \
                ERROR("Rank " << myrank << " line at " << fastq_lines   \
                      << " has more than " << count << " characters "); \
            else                                                        \
                return;                                                 \
        }                                                               \
        if (!len)                                                       \
            return;                                                     \
    } while (0)

// rsize is 0 for incomplete record in the chunk, otherwise set to record size
void H5SeqDB::read_record(char *i, char *s, char *chunk, int chunk_size, 
                          int *rsize, int fail_on_err) 
{
	// make sure this is big enough; should be > ilen + 2
    static char tmp_buf[500] = "";

    *rsize = 0;
	if (chunk_size == 0)
		return;
    int nchar = 0;
	// read idline
	int len = next_fastq_line(i, chunk, ilen);
    CHK_FQ_LEN(len, ilen);

    nchar += len;
	if (nchar >= chunk_size)
		return;
	// check delimiter
	if (i[0] != FASTQ_DELIM) {
        if (fail_on_err)
            ERROR("id line " << fastq_lines << " does not start with '" << FASTQ_DELIM 
                  << "' (" << i[0] << ")");
        else 
            return;
    }
	// read sequence
	len = next_fastq_line(s, chunk + nchar, slen);
    CHK_FQ_LEN(len, slen);

    nchar += len;
	if (nchar >= chunk_size)
		return;
	// check plus
	len = next_fastq_line(tmp_buf, chunk + nchar, ilen + 9);
    CHK_FQ_LEN(len, ilen + 9);

    nchar += len;
	if (nchar >= chunk_size)
		return;
	if (tmp_buf[0] != '+') {
        if (fail_on_err)
            ERROR("expected sequence/quality separator '+' at " << fastq_lines);
        else
            return;
    }
	// read quality
	len = next_fastq_line(s + slen, chunk + nchar, slen);
    CHK_FQ_LEN(len, slen);

    nchar += len;
    *rsize = nchar;
    #ifdef DBG_ON
    Sequence::printFASTQ(stdout, i, s, s + slen, ilen, slen);
	#endif
}

void H5SeqDB::clear_partial(char *chunk, int chunk_size, int *off) 
{
    // This is tricky. A record starts with an @, but those symbols can also 
    // occur anywhere in id lines. And because the chunk can start at any 
    // point, an @ in the middle of a line can appear to be at the start
    // The only way to deal with this problem is to ensure we can read a full 
    // record after getting an @.
    static char id[500], seq[500];
    int rsize = 0;
    for (*off = 0; *off < chunk_size; (*off)++) {
        if (chunk[*off] == FASTQ_DELIM) {
            // we may have a record - don't fail on error
            read_record(id, seq, chunk + *off, chunk_size - *off, &rsize, 0);
            if (rsize) {
                // need to remove spurious addition to fastq_lines from the 
                // read_record call
                fastq_lines -= 4;
                return;
            }
        }
    }
    ERROR("rank " << myrank << " could not find a record in clear partial");
}

void H5SeqDB::mpi_importFASTQ(char *input_name)
{
    fastq_lines = 0;

	// get file size
	struct stat st;
	errno = 0;
	if (stat(input_name, &st) == -1) {
        if (!myrank)
            NOTIFY("could not stat file " << input_name);
        MPI_Finalize();
        exit(EXIT_FAILURE);
    }
	size_t file_size = st.st_size;

    // Work out the maximum number of records. This is useful for estimating
    // progress, and is needed for collective so that the offsets for the
    // different rank write positions don't overlap.  This requires
    // estimating the minimum record size, which is: length of id + length
    // of sequence + plus + length of quality Note: the max records can be
    // computed from the number of lines in the file, and can be passed in
    // as a command line parameter.  But we shouldn't need a super-accurate
    // value here.
    if (max_records == 0) {
        size_t min_rec_size = ilen + slen + 3 + slen;
        max_records = (file_size + min_rec_size - 1) / min_rec_size;
    }

    size_t records_per_rank = (max_records + num_ranks - 1) / num_ranks;
    size_t tenpercent = records_per_rank / 10;

    if (!myrank) {
        double fsize = file_size;
        char fbuf[255];
        if (fsize >= 1024 * 1024 * 1024) 
            sprintf(fbuf, "%.1fGB", fsize / 1024 / 1024 / 1024);
        else if (fsize >= 1024 * 1024) 
            sprintf(fbuf, "%.1fMB", fsize / 1024 / 1024);
        else if (fsize >= 1024) 
            sprintf(fbuf, "%.1fKB", fsize / 1024);
        NOTIFY("Importing FASTQ records from " << input_name << " ("
               << fbuf << ", max " << max_records << " records)");
    }
    if (use_collective_io) {
        // extend here because we could have gaps
        size_t max_extent = records_per_rank * num_ranks;
        hsize_t scount[2] = { max_extent, slen };
        hsize_t icount[2] = { max_extent, ilen };
        H5TRY(H5Dextend(sdset, scount));
        H5TRY(H5Dextend(idset, icount));
    }
    
	MPI_File infile;
	MPI_Status status;
	
	if (MPI_File_open(MPI_COMM_WORLD, input_name, MPI_MODE_RDONLY, 
					  MPI_INFO_NULL, &infile) != 0) {
		if (!myrank)
			cerr << "[" << __func__ << ":" << __LINE__ 
                 << "] Couldn't open file " << input_name << endl;
		MPI_Finalize();
		exit(EXIT_FAILURE);
	}
    
	const int chunk_size = 256 * 1024;
	if (!myrank)
		printf("Using %d chunk size for FASTQ reads\n", chunk_size);
	double t, write_t = 0;
	char *chunk = (char *)malloc(chunk_size + 1);
	if (!chunk)
        ERROR("cannot allocate chunk\n");
	int bytes_read;
    // the offset needs to be for this rank
	MPI_Offset fpos = myrank * file_size / num_ranks;
    MPI_Offset end_fpos = (myrank + 1) * file_size / num_ranks;
    //printf("Rank %d is reading from %lld to %lld\n", myrank, fpos, end_fpos);

    int rsize;
	double start_t = MPI_Wtime();
    char tmp_ibuf[10000], tmp_sbuf[10000];
    int first_chunk = 1;
	// Because we have to read in chunks, we could have a partial record at the 
	// end of a chunk. To deal with this, we abort the read on a partial record,
	// and reset the file pointer to the start of the partial record.
	do {
		// read in chunks
        MPI_Status status;
		MPI_File_read_at(infile, fpos, chunk, chunk_size, MPI_CHAR, &status);
        MPI_Get_count(&status, MPI_CHAR, &bytes_read);
        DBG("fpos %lld, bytes_read %d\n", fpos, bytes_read);
		int offset = 0;
        if (first_chunk) {
            // the first read probably won't be aligned to the start of a record
            clear_partial(chunk, bytes_read, &rsize);
            offset += rsize;
            fpos += rsize;
            first_chunk = 0;
        }
		do {
			hsize_t remainder = nrecords % blocksize;
			read_record(ibuf + ilen * remainder, 
                        sbuf + 2 * slen * remainder, 
                        chunk + offset, bytes_read, &rsize, 1);
			if (!rsize) 
				// reset to get new chunk
				break;
			offset += rsize;
			fpos += rsize;
			nrecords++;
            DBG("%ld: record_size %d, nrecords %ld, offset %d, fpos %lld\n", 
                fastq_lines, rsize, nrecords, offset, fpos);
            if (!myrank && !(nrecords % tenpercent))
                cout << "  " << (10 * nrecords / tenpercent) << "%" << endl;
			if (nrecords % blocksize == 0) {
				// flush if the write buffer is full
				t = MPI_Wtime();
				flush_writes(blocksize);
				write_t += (MPI_Wtime() - t);
			}
            if (fpos >= end_fpos) {
                bytes_read = 0;
                break;
            }
		} while (offset < bytes_read);
	} while (bytes_read == chunk_size);

	MPI_File_close(&infile);

	hsize_t remainder = nrecords % blocksize;
	if (remainder) {
		t = MPI_Wtime();
        flush_writes(remainder);
		write_t += (MPI_Wtime() - t);
	}

    double read_t = MPI_Wtime() - start_t - write_t -parpack_t;

	double elapsed_t = MPI_Wtime() - start_t;
    size_t tot_nrecords, tot_lines;
    double max_read_t, max_write_t, max_pack_t, tot_t;
    MPI_Reduce(&nrecords, &tot_nrecords, 1, MPI_UINT64_T, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&fastq_lines, &tot_lines, 1, MPI_UINT64_T, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&read_t, &max_read_t, 1, MPI_DOUBLE_PRECISION, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&write_t, &max_write_t, 1, MPI_DOUBLE_PRECISION, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&parpack_t, &max_pack_t, 1, MPI_DOUBLE_PRECISION, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&elapsed_t, &tot_t, 1, MPI_DOUBLE_PRECISION, MPI_MAX, 0, MPI_COMM_WORLD);

    if (!myrank) {
        NOTIFY("Read " << tot_lines << " lines");
        NOTIFY(setiosflags(ios::fixed) << setprecision(1) 
               << "Processed " << tot_nrecords << " records (" 
               << ((double)tot_nrecords / tot_t / 1000000) << "M records/sec)");
        // report the max times because those are the limiting factors
        NOTIFY("FASTQ read time: " << max_read_t << "s");
        NOTIFY("SEQDB write time: " << max_write_t << "s");
        NOTIFY("Packing time: " << max_pack_t << "s");
        if (use_collective_io) 
            NOTIFY("Holes from collective IO: " << (max_records - tot_nrecords)
                   << " holes (" 
                   << (100.0 * (double)(max_records - tot_nrecords) / tot_nrecords)
                   << "%)");
    }
}

#endif // MPIPARALLEL

void H5SeqDB::export_block(FILE* f, hsize_t count)
{
	/* Load the next block. */
	flush_reads();
	/*
	const char* i = ibuf;
	const char* s = sbuf;
	// Print each sequence. 
	for (hsize_t j=0; j<count; j++) {
		Sequence::printFASTQ(f, i, s, s + slen, ilen, slen);
		// Advance the buffers. 
		i += ilen;
		s += 2*slen;
	}
	*/
	nread += count;
}

void H5SeqDB::fill_block(vector<string> & seqs, vector<string> & quals, size_t count)
{	
	/* Load the next block. */
	flush_reads();
	const char* i = ibuf;
	const char* s = sbuf;
	/* Print each sequence. */
	for (hsize_t j=0; j<count; j++) 
	{
		// string (const char* s, size_t n);
		// copies the first n characters from the array of characters pointed by s
		seqs.push_back(string(s, slen));
		quals.push_back(string(s+slen, slen));

		/* Advance the buffers. */
		i += ilen;
		s += 2*slen;
	}
	nread += count;
}


void H5SeqDB::fill_block(vector<string> & seqs, vector<string> & quals, vector<string> & ids, size_t count)
{	
	/* Load the next block. */
	flush_reads();
	const char* i = ibuf;
	const char* s = sbuf;
	/* Print each sequence. */
	for (hsize_t j=0; j<count; j++) 
	{
		// string (const char* s, size_t n);
		// copies the first n characters from the array of characters pointed by s
		seqs.push_back(string(s, slen));
		quals.push_back(string(s+slen, slen));
		ids.push_back(string(i, ilen));

		/* Advance the buffers. */
		i += ilen;
		s += 2*slen;
	}
	nread += count;
}

void H5SeqDB::exportFASTQ(FILE* f)
{
    hsize_t perproc = nrecords; // for the serial case

#ifdef MPIPARALLEL
    perproc = nrecords / num_ranks;
    if (myrank == num_ranks - 1)
        perproc = nrecords - (num_ranks - 1) * perproc;
#endif
    double t = omp_get_wtime();

    /* Iterate over the full read blocks. */
    hsize_t nblocks = perproc / blocksize;
    hsize_t tenpercent = nblocks / 10;
    for (hsize_t i = 0; i < nblocks; i++) {
        export_block(f, blocksize);
        if (tenpercent && !(i % tenpercent) && !myrank && i > 0)
            cout << "  " << 10 * i / tenpercent << "%" << endl;
    }

    /* Export any remaining records. */
    hsize_t remainder = perproc % blocksize;

    if (remainder) export_block(f, remainder);

    if (!myrank)
        NOTIFY(setiosflags(ios::fixed) << setprecision(1) 
               << "Extracted " << nrecords << " records (" 
               << ((double)nrecords / (omp_get_wtime() - t) / 1000000) 
               << "M records/sec)");
}


