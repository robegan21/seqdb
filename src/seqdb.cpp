/*
 * SeqDB - storage model for Next Generation Sequencing data
 *
 * Copyright 2011-2014, Brown University, Providence, RI. All Rights Reserved.
 *
 * This file is part of SeqDB.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include "seqdb.h"
#include "h5seqdb.h"

#define PROGNAME "seqdb"
#include "util.h"

using namespace std;

SeqDB* SeqDB::open(const char* filename)
{
	if (H5Fis_hdf5(filename)) {
		size_t blocksize = 1024*1024;
		char* s = getenv("SEQDB_BLOCKSIZE");
		if (s != NULL) {
			blocksize = atoi(s);
			NOTIFY("overriding default HDF5 blocksize to " << blocksize);
		}
		int myrank = 0;
		int init_flag;
		MPI_Initialized(&init_flag);
		if (init_flag) 
			MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
		if (!myrank)
			printf("Using %ld blocksize (in records) for writes\n", blocksize);
		return new H5SeqDB(filename, READ, 0, 0, blocksize);
	} else {
		ERROR("cannot determine storage layer for '" << filename << "'");
	}
}

SeqDB* SeqDB::create(const char* filename, char mode, size_t slen, 
                     size_t ilen, size_t max_records, int use_collective_io)
{
	int myrank = 0;
    #ifdef MPIPARALLEL
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    #endif
	size_t blocksize = 1024*1024;//16*1024;
	char* s = getenv("SEQDB_BLOCKSIZE");
	if (s != NULL) {
		blocksize = atoi(s);
		if (!myrank) 
			NOTIFY("overriding default HDF5 blocksize to " << blocksize);
	}
	if (!myrank)
		NOTIFY("Using " << blocksize << " blocksize (in records) for writes");
	return new H5SeqDB(filename, mode, slen, ilen, blocksize, max_records, 
                       use_collective_io);
}

SeqDB::SeqDB(const char* filename, char mode, size_t slen, size_t ilen, 
             size_t max_records, int use_collective_io)
{
    this->max_records = max_records;
	this->filename = filename;
	this->mode = mode;
	this->slen = slen;
	this->ilen = ilen;

	if ((mode != READ) && (mode != TRUNCATE) && (mode != APPEND)) {
		ERROR("mode must be READ, TRUNCATE, or APPEND");
	}

	qual_offset = 33;
	pack = new SeqPack(slen);
}

SeqDB::~SeqDB()
{
	delete pack;
}

