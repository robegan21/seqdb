/*
 * SeqDB - storage model for Next Generation Sequencing data
 *
 * Copyright 2011-2014, Brown University, Providence, RI. All Rights Reserved.
 *
 * This file is part of SeqDB.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <omp.h>
#ifdef MPIPARALLEL
#include <mpi.h>
#endif

#include "seqdb.h"
#include "fastq.h"

#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: seqdb-compress [-a|-p] SLEN ILEN FASTQ SEQDB\n"
"\n"
"Writes the FASTQ input file into a SEQDB file. Specify '-' for the FASTQ\n"
"file to read from stdin instead.\n"
"\n"
"You must specify a maximum sequence/quality score length (SLEN) and a\n"
"maximum ID length (ILEN).\n"
"\n"
"  -a  append to the SEQDB file instead of truncating\n"
#ifdef MPIPARALLEL
"  -r  <records> the maximum number of records (it will be estimated otherwise)\n"
"  -c  Don't use collective I/O for MPI. Writes to 1 file per rank"
#endif
	<< endl;
}

int main(int argc, char** argv)
{
	double main_t = omp_get_wtime();

	char mode = SeqDB::TRUNCATE;
	size_t slen = 0;
	size_t ilen = 0;
    #ifdef MPIPARALLEL
    int use_collective_io = 1;
    #else
    int use_collective_io = 0;
    #endif
    int myrank = 0;
    int nprocs;
    size_t max_records = 0;

    #ifdef MPIPARALLEL
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	#endif

	//char pname[MPI_MAX_PROCESSOR_NAME + 1];
	//int pname_len;
	//MPI_Get_processor_name(pname, &pname_len);
	//printf("Rank %d is running on processor %s\n", myrank, pname);
    #ifdef MPIPARALLEL
    const char *options = "hvar:c";
    #else
    const char *options = "hva";
    #endif
	int c;
	while ((c = getopt(argc, argv, options)) != -1)
		switch (c) {
        case 'a':
            mode = SeqDB::APPEND;
            break;
        case 'v':
            if (!myrank)
                PRINT_VERSION;
            #ifdef MPIPARALLEL
            MPI_Finalize();
            #endif
            return EXIT_SUCCESS;
        #ifdef MPIPARALLEL
        case 'r':
            max_records = atoi(optarg);
            break;
        case 'c':
            use_collective_io = 0;
            break;
        #endif
        case 'h':
        default:
            if (!myrank)
                print_usage();
            #ifdef MPIPARALLEL
            MPI_Finalize();
            #endif
            return EXIT_SUCCESS;
		}

	if (argc - optind < 4) 
        ARG_ERROR("not enough parameters");
    
	slen = atoi(argv[optind]);
	ilen = atoi(argv[optind+1]);

	if (slen <= 0)
		ARG_ERROR("invalid sequence length (SLEN) " << slen);

	if (ilen <= 0)
		ARG_ERROR("invalid ID length (ILEN) " << ilen);

	/* Open input file. */
	char* input_name = argv[optind+2];

	FASTQ* input;
    #ifdef MPIPARALLEL
    if (!myrank) 
        NOTIFY("Using MPI IO with " << nprocs << "ranks and " 
               << omp_get_max_threads << " threads/rank");
    #else
    if (strcmp(input_name, "-") == 0) {
        NOTIFY("adding FASTQ records from '<stdin>'");
        input = new streamFASTQ("-");
    } else {
        NOTIFY("adding FASTQ records from '" << input_name << "'");
        input = new mmapFASTQ(input_name);
    }
    #endif

	SeqDB* db = SeqDB::create(argv[optind+3], mode, slen, ilen, max_records, 
                              use_collective_io);
	size_t nrecords = db->size();

    #ifdef MPIPARALLEL
    db->mpi_importFASTQ(input_name);
    #else 
    db->importFASTQ(input);
    NOTIFY("added " << (db->size() - nrecords) << " records");
    delete input;
    #endif

	delete db;

    if (!myrank) 
        NOTIFY("Total time taken " << (omp_get_wtime() - main_t));

    #ifdef MPIPARALLEL
    MPI_Finalize();
    #endif

	return EXIT_SUCCESS;
}

