#ifndef __H5SEQDB_H__
#define __H5SEQDB_H__

#include <string>
#include <vector>
#include <hdf5.h>
#include "seqdb.h"

using namespace std;

class H5SeqDB : public SeqDB {

	public:
		/* functions */
        H5SeqDB(const char* filename, char mode, size_t slen, size_t ilen, 
                size_t blocksize, size_t max_records = 0, int use_collective_io = 0);
		~H5SeqDB();

		/* overrides */
		void write(const Sequence& seq);
		bool read(Sequence& seq);
		void readAt(size_t i, Sequence& seq);
		void importFASTQ(FASTQ* f);
        #ifdef MPIPARALLEL
		void mpi_importFASTQ(char *input_name);
        #endif
		void exportFASTQ(FILE* f);
		size_t getBlockSize() { return blocksize;}
		void fill_block(vector<string> & seqs, vector<string> & quals, size_t count);
		void fill_block(vector<string> & seqs, vector<string> & quals, vector<string> & ids, size_t count);

	private:
		/* functions */
		void open_file(const char* filename, hid_t mode);
		void open_datasets(const char* path);
		void read_array_attribute(const char* name, hid_t type, hsize_t n, void* array);
		void create_file(const char* filename, hid_t mode);
		void create_datasets(const char* path);
		void write_array_attribute(const char* name, hid_t type, hsize_t n, const void* array);
		void flush_writes(hsize_t remainder);
		void flush_reads();
		void read_at(size_t i);
		void alloc();
		void clear_buffers();
		void clear_buffers_at();
		void export_block(FILE* f, hsize_t count);
        #ifdef MPIPARALLEL
		int next_fastq_line(char *dest, char *src, int count);
		void read_record(char *i, char *s, char *chunk, int bytes_read, 
                         int *rsize, int fail_on_err);
        void clear_partial(char *chunk, int chunk_size, int *off);
        #endif
		/* data */
		const size_t blocksize;
		/* buffers for block reading and writing */
		uint8_t* pbuf; // packed buffer
		size_t pbufsize;
		char* sbuf; // sequence (unpacked) buffer
		size_t sbufsize;
		char* ibuf; // ID buffer
		size_t ibufsize;
		/* buffers for random-access reads */
		uint8_t* pbuf_at;
		char* sbuf_at;
		char* ibuf_at;
		/* HDF5 objects */
		hid_t h5file;
		hid_t sdset;
		hid_t sspace;
		hid_t smemspace;
		hid_t idset;
		hid_t ispace;
		hid_t imemspace;
        // for mpi
        size_t fastq_lines;
        int myrank;
        int num_ranks;
        int use_collective_io;
};

#endif

